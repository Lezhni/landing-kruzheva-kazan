$(document).ready(function() {

  // init slider
  $('.js-clients').slick();
});

// open popup on click `js-popup` class
$('.js-popup').on('click', function(e) {
	e.preventDefault();

	var target = $(this).attr('href');
	showPopup(target);
});

// scroll to blocks
$('.js-scroll-to').on('click', function(e) {
	e.preventDefault();
	var target = $(this).attr('href');

	if ( typeof target !== 'undefined' ) {
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 1000);
	}
});

// form submiting
$(document).on('submit', 'form', function(e) {
	e.preventDefault();

    var $form = $(this); var errors = false;

    var getQuery = window.location.search.substr(1);
    var formData = $form.serialize() + '&' + getQuery;

    if ( formData ) {
        $.ajax({
        	url: 'order.php',
        	method: 'POST',
        	data: formData,
        	beforeSend: function() {
        		$form.find('[type="submit"]').attr('disabled', 'disabled');
        	},
        	complete: function() {
        		$form.find('[type="submit"]').removeAttr('disabled');
        	},
        	success: function(data) {
	            if ( data == 'sended' ) {
	                $.magnificPopup.close();
	            } else if ( data == 'sended_degustation' ) {
	            	showPopup('#thanks');
	            } else {
	            	console.log(data);
	            }

              $form.find('input[type="text"], input[type="tel"]').val('');

              // var downloadLink = document.createElement('a');
              // downloadLink.download = 'kruzheva_menu.pdf';
              // downloadLink.href = '/kruzheva_menu.pdf';
              // downloadLink.click();
	        }
	    });
    }
});
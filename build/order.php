<?php

if ( $_POST ) {

    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('krubanket@gmail.com', 'Банкет в ресторане Кружева');
    $mail->addReplyTo('krubanket@gmail.com', 'Банкет в ресторане Кружева');
    $mail->addAddress('krubanket@gmail.com');
    $mail->isHTML(true);

    $mail->Subject = $_POST['form_type'];
    $mail->Body = '';

    if ( isset($_POST['contact']) ) $mail->Body .= "Имя: {$_POST['contact']}";
    if ( isset($_POST['phone']) ) $mail->Body .= "<br>Телефон: {$_POST['phone']}";
    if ( isset($_POST['form_type']) ) $mail->Body .= "<br>Форма: {$_POST['form_type']}";
    if ( isset($_POST['utm_source']) ) $mail->Body .= "<br>Откуда: {$_POST['utm_source']}";
    if ( isset($_POST['utm_medium']) ) $mail->Body .= "<br>Тип площадки: {$_POST['utm_medium']}";
    if ( isset($_POST['utm_term']) ) $mail->Body .= "<br>Ключевое слово: {$_POST['utm_term']}";
    if ( isset($_POST['utm_campaign']) ) $mail->Body .= "<br>Кампания: {$_POST['utm_campaign']}";
    if ( isset($_POST['utm_content']) ) $mail->Body .= "<br>Содержание компании: {$_POST['utm_content']}";

    if ( isset($_POST['pm_source']) ) $mail->Body .= "<br>Сайт РСЯ: {$_POST['pm_source']}";
    if ( isset($_POST['pm_block']) ) $mail->Body .= "<br>Блок объявления: {$_POST['pm_block']}";
    if ( isset($_POST['pm_position']) ) $mail->Body .= "<br>Позиция объявления в блоке: {$_POST['pm_position']}";

    if( $mail->send() ) {
        if ( $_POST['form_type'] == 'Запись на дегустацию' ) {
            echo 'sended_degustation';
        } else {
            echo 'sended';
        }
    } else {
        echo $mail->ErrorInfo;
    }
}

die();
